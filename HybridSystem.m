classdef HybridSystem < handle
    %HYBRIDSYSTEM Simulates system with collisions
    %   details...
    
    properties
        Q % Set of discrete states
        q % Current state
        x % (Current) Set of continuous states
        t % Time state (seperate for ease of implementation)
        f % Vector field Q x X -> X
        init_x % Set of initial states X
        init_Q % Set of initial states Q
        %inv % Descrives the invariants Q -> P(X)
        E % Set of edge transitios Q -> Q
        G % Guard condition E -> 2^X
        R % reset map E -> 2^(X x X)
        sens % Sensor that checks unsafe link speeds (only fastest link)
        lLink
        lLinkBot
        widthCart
        heightCart
        widthLink
        xP
        yP
        colLim_gr % Collision against the ground limit
        colLim_int % Collision internally limit
        colLim_int2 % Collision internally limit 2
    end
    
    methods
        function obj = HybridSystem(xCart0, phiRight0, phiMid0, u_, max_omega)
            %HYBRIDSYSTEM Construct an instance of this class
            %   details...
            
            % Create variables
            syms t                                  real
            syms xCart(t)    phiRight(t) phiMid(t)        % First the 3 generalized coordinates
            syms xLeftPiv(t) yLeftPiv(t)                  % Coordinates of the left pivot.
            syms xRightPiv(t) yRightPiv(t)                % Coordinates of the right pivot. 
            syms xBotPiv(t) yBotPiv(t)                    % Coordinates of the bottom pivot. 
            syms xTopLeft(t) yTopLeft(t) phiTopLeft(t)    % Coordinates of the link top left. 
            syms xTopRight(t) yTopRight(t) phiTopRight(t) % Coordinates of the link top right. 
            syms xBotLeft(t) yBotLeft(t) phiBotLeft(t)    % Coordinates of the link bottom left. 
            syms xBotRight(t) yBotRight(t) phiBotRight(t) % Coordinates of the link top right.
            syms vel(t) T(t) V(t) 
            syms i_(t) u(t)
            syms solRight solMid real
            syms bCar real

            assume([xCart(t), phiRight(t), phiMid(t)], 'real');
            assumeAlso([xLeftPiv(t), yLeftPiv(t), xRightPiv(t), 
                        yRightPiv(t), xBotPiv(t), yBotPiv(t)], 'real');
            assumeAlso([xTopLeft(t), yTopLeft(t), phiTopLeft(t)], 'real');
            assumeAlso([xTopRight(t), yTopRight(t), phiTopRight(t)], 'real');
            assumeAlso([xBotLeft(t), yBotLeft(t), phiBotLeft(t)], 'real');
            assumeAlso([xBotRight(t), yBotRight(t), phiBotRight(t)], 'real');
            assumeAlso([vel(t), T(t), V(t)], 'real'); 
            assumeAlso([i_(t) u(t)], 'real'); 
            
            % Given constants
            mCart = 0.2; % kg
            mLink = 0.1; %kg
            mLinkBot = 0.05; %kg
            mMass = 0.1; %kg; 
            bJoints = 0.001; % Dampening coefficient joints (simulate friction)
            bCar_val = 0.01; % Dampening coefficient car (simulate friction)
            g = 9.81;

            lLink = 0.5; %m
            obj.lLink = lLink;
            lLinkBot = 0.25;%; % m
            obj.lLinkBot = lLinkBot;

            ILink = 1/12 * mLink * lLink^2;
            ILinkBot = 1/12 * mLinkBot * lLinkBot^2;

            k = 5; %N/m % 5
            LSpring = 0.55; %m %0.5

            R = 0.3; %Ohm
            Lind = 0.01; %Henry
            Kp = 1; %N/A
            
            rho = 0.8;
            
            
            obj.colLim_gr = 0.1+0.05; % Collision against the ground limit [m]
            obj.colLim_int = 0.1; % Collision internally limit [rad]
            obj.colLim_int2 = asin(lLinkBot/lLink)-0.01; % Collision internally limit 2 [rad]
            
            % Define hybrid automaton components
            obj.Q = {'motor_on'; 'motor_off'};
            obj.init_x = [xCart0; phiRight0; phiMid0; 0; 0; 0; 0]; % 3- i, vcart, omegaRight, omegaMid
            obj.init_Q = obj.Q{1};
            obj.x = obj.init_x;
            obj.q = obj.init_Q;
            obj.sens = SensorAlarm(max_omega, 0.3); % W = max_omega, flip_prob = 30%
            g1 = @(x, t)(abs(x(2)) + abs(x(3)) > 0.5*pi-asin(obj.colLim_gr/lLink));
            g2 = @(x, t)(x(2) < obj.colLim_int || x(2) > obj.colLim_int2);
            g3 = @(x, t)(t > 2);
            g4 = @(x, t)(obj.sens.checkAlarm());
            obj.G = {g1; g2; g3; g4};
            col = CollisionHandler2(lLink, lLinkBot, solRight, solMid, rho);
            r1 = @(x, t)(col.collisionWall(x, t, 0.5*pi-asin(obj.colLim_gr/lLink)));
            r2 = @(x, t)(col.collisionInternal(x, t));
            r3 = @(x, t)([obj.x; 0]);
            r4 = @(x, t)([obj.x; 0]);
            obj.R = {r1; r2; r3; r4};
            obj.E = {{obj.Q{1}, obj.Q{1}, obj.G{1}, obj.R{1}};
                     {obj.Q{1}, obj.Q{1}, obj.G{2}, obj.R{2}};
                     {obj.Q{1}, obj.Q{2}, obj.G{4}, obj.R{4}};
                     {obj.Q{2}, obj.Q{2}, obj.G{4}, obj.R{4}};
                     {obj.Q{2}, obj.Q{2}, obj.G{1}, obj.R{1}};
                     {obj.Q{2}, obj.Q{2}, obj.G{2}, obj.R{2}};
                     {obj.Q{2}, obj.Q{1}, obj.G{3}, obj.R{3}};}; % Set of edge transitions (containing G and R)
            
            % Help variables
            obj.heightCart = 0.2; % m
            obj.widthCart = 0.2; % m
            obj.widthLink = 0.05; % m

            massRadius = 0.05;
            tP = linspace(0, 2*pi);
            obj.xP = massRadius*cos(tP);
            obj.yP = massRadius*sin(tP);

            % Coordinates 
            xLeftPiv = xCart + lLink * sin(-phiRight + phiMid);
            yLeftPiv = -lLink * cos(-phiRight + phiMid);

            xRightPiv = xCart + lLink * sin(phiMid + phiRight);
            yRightPiv = -lLink * cos(phiMid + phiRight);

            phiTopLeft = -phiRight + phiMid;
            xTopLeft = xCart + 0.5 * lLink * sin(phiTopLeft);
            yTopLeft = -0.5 * lLink * cos(phiTopLeft);

            phiTopRight = phiMid + phiRight;
            xTopRight = xCart + 0.5 * lLink * sin(phiTopRight);
            yTopRight = -0.5 * lLink * cos(phiTopRight);

            phiBot = asin(lLink*sin(phiRight)/lLinkBot);

            phiBotLeft = phiMid + phiBot; % phiTopRight;
            xBotLeft = xLeftPiv + 0.5 * lLinkBot * sin(phiBotLeft);
            yBotLeft = yLeftPiv - 0.5 * lLinkBot * cos(phiBotLeft);

            phiBotRight = phiMid - phiBot; % phiTopLeft;
            xBotRight = xRightPiv + 0.5 * lLinkBot * sin(phiBotRight);
            yBotRight = yRightPiv - 0.5 * lLinkBot * cos(phiBotRight);

            xBotPiv = xLeftPiv + lLinkBot * sin(phiBotLeft);
            yBotPiv = yLeftPiv - lLinkBot * cos(phiBotLeft);

            % Energies
            inertiamatrix = [mCart;
                              mLink;mLink;ILink;
                              mLink;mLink;ILink;
                              mLinkBot;mLinkBot;ILinkBot;
                              mLinkBot;mLinkBot;ILinkBot;
                              mMass;mMass;
                              mMass;mMass;
                              mMass;mMass];

            dist = [xCart;
                    xTopLeft;yTopLeft; phiTopLeft;
                    xTopRight;yTopRight;phiTopRight;
                    xBotRight;yBotRight;phiBotRight;
                    xBotLeft;yBotLeft; phiBotLeft;
                    xLeftPiv; yLeftPiv;
                    xRightPiv; yRightPiv;
                    xBotPiv; yBotPiv];
            vel = diff(dist);
            T = simplify(1/2*inertiamatrix'*vel.^2, 10);
            V = simplify(1/2*k*(norm([xLeftPiv - xRightPiv;yLeftPiv-yRightPiv])-LSpring)^2 + ...
                         mMass * g * (yLeftPiv + yRightPiv + yBotPiv)+...
                         mLink * g * (yTopLeft + yTopRight)+...
                         mLinkBot * g * (yBotLeft + yBotRight));

            relVel = diff([xCart; %vCart
                          phiTopLeft; %omegaTopLeft with relation to cart
                          phiTopRight; %omegaTopRight with relation to cart
                          phiTopLeft - phiTopRight; %Difference between TL and TR
                          phiTopLeft - phiBotLeft;  %Difference between TL and BL
                          phiTopRight - phiBotRight;  %Difference between TR and BR
                          phiBotRight - phiBotLeft]);  %Difference between BL and BR      
            N = [bCar; bJoints; bJoints;
                 bJoints; bJoints; bJoints; bJoints];

            D = simplify(1/2 * N' * relVel.^2);
            % Lagrange's equations of motion
            L = T - V;

            EoMe = Lind * diff(i_(t), t) + i_(t) * R + Kp * diff(xCart(t), t) - u(t);
            EoM1 = diff(diff(L(t), diff(xCart(t), t)), t) - diff(L(t), xCart(t)) + diff(D(t), diff(xCart(t), t)) - Kp * i_(t);
            EoM2 = diff(diff(L(t), diff(phiRight(t), t)), t) - diff(L(t), phiRight(t)) + diff(D(t), diff(phiRight(t), t));
            EoM3 = diff(diff(L(t), diff(phiMid(t), t)), t) - diff(L(t), phiMid(t)) + diff(D(t), diff(phiMid(t), t));

            EoM = [EoM1; EoM2; EoM3; EoMe];
            
            % Reduce order differential equation
            [eq, var] = reduceDifferentialOrder(EoM, [xCart, phiRight, phiMid, i_]);
            %F = simplify(F);
            [M,F] = massMatrixForm(eq, var);
            f = inv(M)*F; % Do not replace with M\F!
            
            % Create function with input
            u = u_;
            bCar = bCar_val; % Brake off
            fu1 = subs(f);
            ode1 = odeFunction(fu1,var);

            u = @(t)(0); % Zero input for safety state
            bCar = 10; % Dampening coefficient car increased to sim hydraulic brake
            fu2 = subs(f);
            ode2 = odeFunction(fu2,var);
            
            obj.f = {ode1, ode2};
        end
        
        function [y, tsim, m_state] = simulate(obj, sim_t)
            %SIMULATE simulates the hybrid system
            %   Solve ODE
            tsim = 0:0.01:sim_t;
            dt = tsim(2);
            m_state = zeros(1, length(tsim));
            m_state(1) = strcmp(obj.q, obj.Q{1});
            y = zeros(length(obj.x), length(tsim));
            y(:, 1) = obj.x;
            for h = 2:length(tsim)
                obj.x = y(:, h-1);
                ti = tsim(h);
                obj.t = obj.t + dt;
                % Update current state by checking edges
                state_edges = {};
                for edge = 1:size(obj.E, 1)
                    if strcmp(obj.q, obj.E{edge}{1})
                        state_edges{end+1} = obj.E{edge};
                    end
                end
                for e = 1:length(state_edges)
                    g = state_edges{e}{3};
                    if g(obj.x, obj.t)
                        obj.q = state_edges{e}{2};
                        r = state_edges{e}{4};
                        x_new = r(obj.x, obj.t);
                        obj.x = x_new(1:7);
                        obj.t = x_new(8);
                    end
                end
                % Simulate one step based on current state
                for state = 1:length(obj.Q)
                    if strcmp(obj.q, obj.Q{state})
                        y(:, h) = obj.RK4(obj.f{state}, obj.x, ti, dt);
                    end
                end
                % Update the sensor based on the fastest joint
                obj.sens.filterSensedOmega(abs(obj.x(6))+abs(obj.x(7)));
                
                % Store the state
                m_state(h) = strcmp(obj.q, obj.Q{1});
            end
        end 
        
        function reset(obj)
            %RESET Resets the simulation
            %  Resets the simulation to its initial consitions
            obj.x = obj.init_x;
            obj.q = obj.init_Q;
            obj.t = 0;
            obj.sens.reset();
        end
        
        function y = RK4(obj, ODE, xi, ti, dt)
            %RK4 Apply Runga-Kutta algorithm to solve first order ODE
            %   One step RK4
            k1 = dt * ODE(ti, xi);
            k2 = dt * ODE(ti+dt/2, xi + k1/2);
            k3 = dt * ODE(ti+dt/2, xi + k2/2);
            k4 = dt * ODE(ti + dt, xi + k3);
            y = xi + 1/6 * (k1 + 2* k2 + 2*k3 + k4);
        end
        
        function animate(obj, solRK, tRK, q, simspeed)
            figure('Name', 'Animation')
            % Animate for all points (calculations)
            xC = solRK(1,:);
            phiR = solRK(2,:);
            phiM = solRK(3,:);
            
            [CoM, patches] = Coordinates(xC,phiR, phiM, obj.lLink, ...
                obj.lLinkBot, obj.widthCart, obj.heightCart, obj.widthLink);

            % Plot animation
            axis equal;
            x0=0.1;
            y0=0.1;
            width=0.8;
            height=0.8;
            set(gcf, 'units', 'normalized');
            set(gcf,'position',[x0,y0,width,height])
            plot([min(CoM(1,:))-2, max(CoM(1,:))+2], [-obj.colLim_gr+0.05,-obj.colLim_gr+0.05], '-k')
            ylim([-1.5, 0.5]);
            for j = 1:floor(length(tRK)/simspeed)
                j = floor(simspeed*j);
                xlim(CoM(1,j) + [-2, 2]);
                delete(findobj('type', 'patch'));
                
                if(q(j)) % Update the color of the cart based on the motor
                    cart_color = 'green';
                else
                    cart_color = 'black';
                end
                patch(patches(1, (j-1)*4 + 1:j*4), patches(2,(j-1)*4 + 1:j*4), cart_color);
                patch(patches(3, (j-1)*4 + 1:j*4), patches(4,(j-1)*4 + 1:j*4), 'red');
                patch(patches(5, (j-1)*4 + 1:j*4), patches(6,(j-1)*4 + 1:j*4), 'red');
                patch(patches(7, (j-1)*4 + 1:j*4), patches(8,(j-1)*4 + 1:j*4), 'red');
                patch(patches(9, (j-1)*4 + 1:j*4), patches(10,(j-1)*4 + 1:j*4), 'red');
                patch(obj.xP + CoM(10,j), obj.yP + CoM(13,j), 'blue');
                patch(obj.xP + CoM(11,j), obj.yP + CoM(14,j), 'blue');
                patch(obj.xP + CoM(12,j), obj.yP + CoM(15,j), 'blue');
                patch([CoM(1,j)], [0], [0], 'Marker', 'o', 'MarkerFaceColor','flat');
                patch([CoM(2,j)], [CoM(6,j)], [0], 'Marker', 'o', 'MarkerFaceColor','flat');
                patch([CoM(3,j)], [CoM(7,j)], [0], 'Marker', 'o', 'MarkerFaceColor','flat');
                patch([CoM(4,j)], [CoM(8,j)], [0], 'Marker', 'o', 'MarkerFaceColor','flat');
                patch([CoM(5,j)], [CoM(9,j)], [0], 'Marker', 'o', 'MarkerFaceColor','flat');
                patch([CoM(10,j)], [CoM(13,j)], [0], 'Marker', 'o', 'MarkerFaceColor','flat');
                patch([CoM(11,j)], [CoM(14,j)], [0], 'Marker', 'o', 'MarkerFaceColor','flat');
                patch([CoM(12,j)], [CoM(15,j)], [0], 'Marker', 'o', 'MarkerFaceColor','flat');

                pause(0.0001);
            end
        end
        
    end
end

