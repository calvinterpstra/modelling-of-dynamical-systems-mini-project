clc; clear; close all;

%% Construct the sensor object
sens = SensorAlarm(1, 0.3); % W = 1, flip_prob = 30%
sens_perf = SensorAlarm(1, 0); % W = 1, flip_prob = 0%

%% Test inputs
% Define test inputs
omega0 = [0,0,0,0,0,0,0,0]; % Always safe
omega1 = [0,0,0,0,0,1,1,1]; % Sometimes doubt
omega2 = [0,0,0,1,1,1,1,1]; % Always doubt
omega3 = [0,0,1,1,1,1,1,1]; % Certain T (for perfect sensor)
omegaS1 = [0,1,1,1,0,1,1,1]; % Special case 1
omegaS2 = [1,1,1,0,1,1,1,0]; % Special case 2
omegaS3 = [0,1,1,1,1,1,1,0]; % Special case 3
omegaS4 = [1,1,1,1,1,1,0,0]; % Special case 4
omegaS5 = [1,1,1,0,0,1,1,1]; % Special case 5
omega4 = [1,1,1,1,1,1,1,1,1,1,1,1,1]; % All 1s
omega5 = [1,1,1,1,1,1,0,0,0,0,0,0,0]; % Certain T, then to safe
% Simulate test sequences
clc;
iterations = 1;
sensor = sens_perf;
t0 = simulate(sensor, omega0, iterations)
t1 = simulate(sensor, omega1, iterations)
t2 = simulate(sensor, omega2, iterations)
t3 = simulate(sensor, omega3, iterations)
s1 = simulate(sensor, omegaS1, iterations)
s2 = simulate(sensor, omegaS2, iterations)
s3 = simulate(sensor, omegaS3, iterations)
s4 = simulate(sensor, omegaS4, iterations)
s5 = simulate(sensor, omegaS5, iterations)
t4 = simulate(sensor, omega4, iterations)
t5 = simulate(sensor, omega5, iterations)

t6 = simulate_last_only(sens, omega3, 5)


%% Functions
function outputs = simulate(sensor, omega, iter)
    outputs = {};
    for j = 1:iter
        outputs{j} = sensor.filterSensedOmega(omega(1));
        for i = 2:length(omega)
            outputs{j} = [outputs{j}, sensor.filterSensedOmega(omega(i))];
        end
        sensor.reset();
    end
end

function output = simulate_last_only(sensor, omega, iter)
    output = {};
    for j = 1:iter
        for i = 1:length(omega)-1
            sensor.filterSensedOmega(omega(i));
        end
        output{j} = sensor.filterSensedOmega(omega(end));
        sensor.reset();
    end
end



