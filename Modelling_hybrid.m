disp('Analysis of mechanism')
clear all; clc; close all;

%% Init hybrid system
freq = 1/2; % Hz
A = 3.5; % Amplitude
u = @(t)(A*sin(2 * pi * freq * t)); % Create motor input

hsys = HybridSystem(0, 0.2, 0, u, 6);

%% Simulate system
clc;
hsys.reset();
[sol, t, q] = hsys.simulate(60);

%% Animation
hsys.animate(sol, t, q, 4);

%% Plot states
figure('Name', 'Plot generalized coordinates')
plot(t, sol(1,:)); grid on; hold on;
plot(t, sol(2,:));
plot(t, sol(3,:)); 
% plot(t, sol(4, :)); hold off;
legend('xCar', 'phiRight', 'phiMid'); %, 'current'

%% Test internal collisions
u = @(t)(0); % Create motor input
hsys = HybridSystem(0, 0.5, 0, u, 100);
clc;
hsys.reset();
[sol, t, q] = hsys.simulate(5);
%%
% hsys.animate(sol, t, q, 4);
figure('Name', 'Test internal collisions')
plot(t, sol(2,:)); grid on;
legend('$\phi_{right}, \rho = 0.5$', 'interpreter', 'latex');
title("Test Internal Collisions")
xlabel('Time (sec)', 'interpreter', 'latex') 
ylabel('$\phi_{right}$ (rad)', 'interpreter', 'latex') 

%% Test ground collisions
u = @(t)(0); % Create motor input
hsys = HybridSystem(0, 0.3, 0, u, 100);
clc;
hsys.reset();
[sol, t, q] = hsys.simulate(2);
%%
% hsys.animate(sol, t, q, 4);
figure('Name', 'Test ground collisions')
plot(t, abs(sol(6,:))+abs(sol(7,:))); grid on; hold on
plot(t, abs(sol(2,:))+abs(sol(3,:)));
legend('$|\dot{\phi}_{mid}| + |\dot{\phi}_{right}|, \rho = 1$', ...
    '$|\phi_{mid}| + |\phi_{right}|, \rho = 1$', 'interpreter', 'latex');
title("Test Ground Collisions")
xlabel('Time (sec)', 'interpreter', 'latex') 
ylabel('Magnitude of fastest joint speed (rad/s) and position (rad)', ...
    'interpreter', 'latex') 

%% Test sensor alarm
freq = 1/1; % Hz
A = 2; % Amplitude
u = @(t)(A*sin(2 * pi * freq * t)); % Create motor input
hsys = HybridSystem(0, 0.5, 0, u, 5);
clc;
hsys.reset();
[sol, t, q] = hsys.simulate(10);
%%
% hsys.animate(sol, t, q, 4);
figure('Name', 'Plot generalized coordinates')
plot(t, abs(sol(6,:))+abs(sol(7,:))); grid on; hold on;
plot(t, q(:));
plot([min(t), max(t)], [5,5], '--k');
legend('$|\dot{\phi}_{mid}| + |\dot{\phi}_{right}|$', 'Motor state', '$W$', ...
    'interpreter', 'latex');
title("Test Sensor Alarm")
xlabel('Time (sec)', 'interpreter', 'latex') 
ylabel('Magnitude of fastest joint speed (rad/s) and state (-)', ...
    'interpreter', 'latex') 

