classdef CollisionHandler2
    %COLLISIONHANDLER2 Handles the collision dynamics
    
    properties
        L1
        L2
        solRight
        solMid
        rho
    end
    
    methods
        function obj = CollisionHandler2(LinkTop, LinkBot, solRight, solMid, rho)
            %COLLISIONHANDLER2 Construct an instance of this class
            %   Input:
            %       - LinkTop: Length of top link
            %       - LinkBot: Length of bottom link
            %       - solRight: Symbolic variable for omegaRight. 
            %       - solMid: Symbolic variable for omegeMid.
            %       - rho: Coefficient of restitution
            obj.L1 = LinkTop;
            obj.L2 = LinkBot;
            obj.solRight = solRight;
            obj.solMid = solMid; 
            obj.rho = rho;
        end
        
        function x_new = collisionWall(obj, x, t, theta_lim) % phiRight=x(2), phiMid=x(3), omeRight=x(6), omeMid=x(7)
            phiRight = x(2);
            phiMid = x(3);
            omeRight = x(6);
            omeMid = x(7);
            if(phiRight + phiMid > 0)
                % Hit wall with right pivot
                v_hit = obj.L1 * sin(phiMid + phiRight)*(omeMid + omeRight);
                eq1 = obj.solMid + obj.solRight == -obj.rho * (omeMid + omeRight);
            else
                % Hit wall with left pivot
                v_hit = obj.L1 * sin(phiMid - phiRight)*(omeMid - omeRight);
                eq1 = obj.solMid - obj.solRight == -obj.rho * (omeMid - omeRight);
            end
            
            vyB = obj.L2*sin(asin((obj.L1*sin(phiRight))/obj.L2) + phiMid)*...
                (omeMid + (obj.L1*cos(phiRight)*omeRight)/...
                (obj.L2*(1 - (obj.L1^2*sin(phiRight)^2)/obj.L2^2)^(1/2))) +...
                obj.L1*sin(phiMid - phiRight)*(omeMid - omeRight);
            
            v2ydes = vyB - (1+obj.rho) * v_hit;
            eq_vy = vpa(obj.L2*sin(asin((obj.L1*sin(phiRight))/obj.L2) + phiMid)*...
                (obj.solMid + (obj.L1*cos(phiRight)*obj.solRight)/...
                (obj.L2*(1 - (obj.L1^2*sin(phiRight)^2)/obj.L2^2)^(1/2))) +...
                obj.L1*sin(phiMid - phiRight)*(obj.solMid - obj.solRight));
            
            eq2 = eq_vy == v2ydes;

            [A, B] = equationsToMatrix([eq1, eq2], [obj.solMid, obj.solRight]);
            X = linsolve(A,B);
            omeMnew = double(X(1));
            omeRnew = double(X(2));
            if(phiRight + phiMid > 0)
                x_new = [1;1;1;1;1;0;0] .* x + [zeros(5, 1); omeRnew; omeMnew] - ...
                    (sum(x(2:3)) - theta_lim) * [0; 0.5; 0.5; zeros(4, 1)];
                disp('Collision! (+)');
            else
                x_new = [1;1;1;1;1;0;0] .* x + [zeros(5, 1); omeRnew; omeMnew] - ...
                    (x(3) - x(2) - -theta_lim) * [0; -0.5; 0.5; zeros(4, 1)];
                disp('Collision! (-)');
            end
            x_new(8) = t;
        end
        
        function x_new = collisionInternal(obj, x, t) % omeRight = x(6)
            omeRnew = -obj.rho * x(6);
            x_new = x;
            x_new(6) = omeRnew;
            if(x(2) < 0.3)
                x_new(2) = 0.1;
                disp('Collision! internal (-)');
            else
                x_new(2) = asin(obj.L2/obj.L1)-0.01;
                disp('Collision! internal (+)');
            end
            x_new(8) = t;
        end
    end
end

