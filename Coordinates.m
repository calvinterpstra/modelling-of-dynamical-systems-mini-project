function [CoM,pat] = Coordinates(xCart, phiRight, phiMid, lLinkTop, lLinkBot, widthCart, heightCart, widthLink)
%COORDINATES Return the coordinates of all CoM of links and pivots + the
%corners required for drawing them. 
%
%   Inputs:
%       - xCart (1 x n):      The x-coordindates of the cart for all timepoints.
%       - phiRight (1 x n):   The angle phiRight for all timepoints.
%       - phiMid (1 x n):     The angle phiMid for all timepoints. 
%       - lLinkTop (1 x 1):   Length of top two links. 
%       - lLinkBot (1 x 1):   Length of bottom two links.
%       - widthCart(1 x 1):   Width of the cart used for drawing the cart. 
%       - heightCart (1 x 1): Height of the cart used for drawing the cart.
%       - widthLink (1 x 1):  Width of the link used for drawing the link. 
%
%   Outputs:
%       - CoM (15 x n): The x,y-coordinates of all CoM for all timepoints.
%                       [xCart: x-coordinate of cart;
%                          xTL: x-coordinate of bottom left link; 
%                          xTR: x-coordinate of bottom right link;  
%                          xBL: x-coordinate of top left link;  
%                          xBR: x-coordinate of top right link;  
%                          yTL: y-coordinate of bottom left link;  
%                          yTR: y-coordinate of bottom right link;  
%                          yBL: y-coordinate of top left link;  
%                          yBR: y-coordinate of top right link; 
%                          xLP: x-coordinate left pivot;
%                          xRP: x-coordinate right pivot;
%                          xBP: x-coordinate bottom pivot;
%                          yLP: y-coordinate left pivot; 
%                          yRP: y-coordinate right pivot;
%                          yBP: y-coordinate bottom pivot]
%
%       - pat (10 x 4n): The x,y coordinates for all corners required to
%                        draw the model. All blocks are ordered as:
%                        [x1, x2, x3, x4;
%                         y1, y2, y3, y4];
%                        and with five blocks for each timestep:
%                        [block_cart: coordinates for cart; 
%                         block_tl: coordinates for top left link; 
%                         block_tr: coordinates for top right link; 
%                         block_bl: coordinates for bottom left link; 
%                         block_br: coordinates for bottom right link];

CoM = zeros(15, length(xCart));
pat = zeros(10, 4*length(xCart));

% Bottom angle
phiBot = asin(lLinkTop*sin(phiRight)/lLinkBot);

% xCart
CoM(1, :) = xCart;

% First pivots as they help with bottom links.
% xLeftPivot
CoM(10, :) = xCart + lLinkTop * sin(phiMid - phiRight);
% xRightPivot
CoM(11, :) = xCart + lLinkTop * sin(phiMid + phiRight);
% xBotPivot
CoM(12, :) = CoM(10, :) + lLinkBot * sin(phiBot + phiMid);

% yLeftPivot
CoM(13, :) = - lLinkTop * cos(phiMid - phiRight);
% yRightPivot
CoM(14, :) = - lLinkTop * cos(phiMid + phiRight);
% yBotPivot
CoM(15, :) = CoM(13, :) - lLinkBot * cos(phiBot + phiMid);

% xTopLeft
CoM(2, :) = xCart + 0.5 * lLinkTop * sin(phiMid - phiRight);
% xTopRight
CoM(3, :) = xCart + 0.5 * lLinkTop * sin(phiMid + phiRight);
% xBotLeft
CoM(4, :) = CoM(10, :) + 0.5 * lLinkBot * sin(phiMid + phiBot);
% xBotRight
CoM(5, :) = CoM(11, :) + 0.5 * lLinkBot * sin(phiMid - phiBot);

% yTopLeft
CoM(6, :) = -0.5 * lLinkTop * cos(phiMid - phiRight);
% yTopRight
CoM(7, :) = -0.5 * lLinkTop * cos(phiMid + phiRight);
% yBotLeft
CoM(8, :) = CoM(13, :) - 0.5 * lLinkBot * cos(phiMid + phiBot);
% yBotRight
CoM(9, :) = CoM(14, :) - 0.5 * lLinkBot * cos(phiMid - phiBot);


cosTL = cos(phiMid - phiRight);
sinTL = sin(phiMid - phiRight);
cosTR = cos(phiMid + phiRight);
sinTR = sin(phiMid + phiRight);

cosBL = cos(phiMid + phiBot);
sinBL = sin(phiMid + phiBot);
cosBR = cos(phiMid - phiBot);
sinBR = sin(phiMid - phiBot);

kroncos = [1 0; 0 1];
kronsin = [0 -1; 1 0];

R_TL = kron(cosTL, kroncos) + kron(sinTL, kronsin);
R_TR = kron(cosTR, kroncos) + kron(sinTR, kronsin);
R_BL = kron(cosBL, kroncos) + kron(sinBL, kronsin);
R_BR = kron(cosBR, kroncos) + kron(sinBR, kronsin);

kron_term = ones(1, length(xCart));

lTop = lLinkTop + 0.04;
lBot = lLinkBot + 0.04;

shapeCart = [-widthCart, widthCart, widthCart, -widthCart; 
             -heightCart, -heightCart, heightCart, heightCart];
         
shapeTop = [-widthLink, widthLink, widthLink, -widthLink; ...
            -lTop, -lTop, lTop, lTop];
        
shapeBot = [-widthLink, widthLink, widthLink, -widthLink; ...
            -lBot, -lBot, lBot, lBot];
    
pat(1:2, :) = kron([xCart; zeros(1, length(xCart))], [1, 1, 1, 1]) + 0.5 * kron(kron_term, shapeCart); 
% Top left
pat(3:4, :) = kron([CoM(2, :); CoM(6, :)], [1, 1, 1, 1]) + 0.5 * R_TL * kron(eye(length(xCart)), shapeTop);

% Top right
pat(5:6, :) = kron([CoM(3, :); CoM(7, :)], [1, 1, 1, 1]) + 0.5 * R_TR * kron(eye(length(xCart)), shapeTop);

% Bottom left
pat(7:8, :) = kron([CoM(4, :); CoM(8, :)], [1, 1, 1, 1]) + 0.5 * R_BL * kron(eye(length(xCart)), shapeBot);


% Bottom right
pat(9:10, :) = kron([CoM(5, :); CoM(9, :)], [1, 1, 1, 1]) + 0.5 * R_BR * kron(eye(length(xCart)), shapeBot);
end

