classdef SensorAlarm < handle
    %SENSORALARM Angular velocity sensor object
    %   The sensors produce an output of 0 when the angular velocity of the
    %   link is within some safe bounds |ω| < W, and a 1 when those bounds
    %   are exceeded. An alarm is signalled if the sensor measures at least
    %   3 consecutive 1s. Assume that it is a low-quality sensor with an
    %   average error rate of less or equal to 1 bits every 8 bits, that
    %   is, in every window of 8 bits there is at most 1 bit flipped from
    %   its actual value. A Moore machine filters the signal from
    %   the sensor and producing a symbol T when it is certain that the
    %   sensor signalled an alarm, D when in doubt, and O when velocities
    %   are certainly within safe bounds.
    
    properties
        W % Safe angular velocity bound
        flip_count % A count of the number of bits since last flipped
        flip_prob % Probability of bit flipping for flip_count >= 8
        %Sigma % Input alphabet (not used as it is simply a boolean)
        Gamma % Output alphabet; T: alarm, D: doubt, O: safe
        Q % Set of states
        Q_0 % Set of initial states
        q % current state
        delta % State-transition relation (Q x Sigma x Q)
        H % Output map
        h % Last output
        freq % Output frequency (bits/s) (not used, as the system_freq = freq)
    end
    
    methods
        function obj = SensorAlarm(W, flip_prob)
            %SENSORALARM Construct an instance of this class
            %   Initialized the predifined Moore machine, with given safe
            %   angular velocity bound and bit flip probability. Since a
            %   bit only has a probability of flipping after 7 bits since
            %   the last flip, the sensor is initialized with a random flip
            %   count (between 0 and 8) to prevent that the first 7 bits
            %   will never have a chance of flipping.
            obj.W = W; % Safe angular velocity bound
            obj.flip_count = randi(9)-1; % Random number between 0 and 8
            obj.flip_prob = flip_prob; % Probability of bit flipping for flip_count >= 8
            obj.Gamma = {'T', 'D', 'O'};
            obj.Q = {'1', '2', '3', '4', '5' ...
                , '6', '7', '8', '9', '10' ...
                , '11', '12', '13', '14', '15' ...
                , '16', '17', '18', '19', '20' ...
                , '21', '22', '23', '24', '25' ...
                , '26', '27', '28', '29', '30' ...
                , '31', '32', '33', '34', '35' ...
                , '36', '37', '38', '39'}; % Set of states
            obj.Q_0 = obj.Q{1}; % Initial state = 'safe'
            obj.q = obj.Q_0; % Current state is the initial state
            obj.delta = {{obj.Q(1), 0, obj.Q(3)};
                         {obj.Q(1), 1, obj.Q(2)};
                         {obj.Q(2), 0, obj.Q(14)};
                         {obj.Q(2), 1, obj.Q(7)};
                         {obj.Q(3), 0, obj.Q(3)};
                         {obj.Q(3), 1, obj.Q(4)};
                         {obj.Q(4), 0, obj.Q(14)};
                         {obj.Q(4), 1, obj.Q(5)};
                         {obj.Q(5), 0, obj.Q(23)};
                         {obj.Q(5), 1, obj.Q(6)};
                         {obj.Q(6), 0, obj.Q(17)};
                         {obj.Q(6), 1, obj.Q(8)};
                         {obj.Q(7), 0, obj.Q(23)};
                         {obj.Q(7), 1, obj.Q(6)};
                         {obj.Q(8), 0, obj.Q(17)};
                         {obj.Q(8), 1, obj.Q(9)};
                         {obj.Q(9), 0, obj.Q(17)};
                         {obj.Q(9), 1, obj.Q(10)};
                         {obj.Q(10), 0, obj.Q(11)};
                         {obj.Q(10), 1, obj.Q(10)};
                         {obj.Q(11), 0, obj.Q(12)};
                         {obj.Q(11), 1, obj.Q(13)};
                         {obj.Q(12), 0, obj.Q(32)};
                         {obj.Q(12), 1, obj.Q(21)};
                         {obj.Q(13), 0, obj.Q(15)};
                         {obj.Q(13), 1, obj.Q(24)};
                         {obj.Q(14), 0, obj.Q(1)};
                         {obj.Q(14), 1, obj.Q(15)};
                         {obj.Q(15), 0, obj.Q(16)};
                         {obj.Q(15), 1, obj.Q(5)};
                         {obj.Q(16), 0, obj.Q(36)};
                         {obj.Q(16), 1, obj.Q(15)};
                         {obj.Q(17), 0, obj.Q(18)};
                         {obj.Q(17), 1, obj.Q(20)};
                         {obj.Q(18), 0, obj.Q(32)};
                         {obj.Q(18), 1, obj.Q(21)};
                         {obj.Q(19), 0, obj.Q(14)};
                         {obj.Q(19), 1, obj.Q(5)};
                         {obj.Q(20), 0, obj.Q(16)};
                         {obj.Q(20), 1, obj.Q(24)};
                         {obj.Q(21), 0, obj.Q(22)};
                         {obj.Q(21), 1, obj.Q(25)};
                         {obj.Q(22), 0, obj.Q(38)};
                         {obj.Q(22), 1, obj.Q(15)};
                         {obj.Q(23), 0, obj.Q(27)};
                         {obj.Q(23), 1, obj.Q(15)};
                         {obj.Q(24), 0, obj.Q(23)};
                         {obj.Q(24), 1, obj.Q(28)};
                         {obj.Q(25), 0, obj.Q(23)};
                         {obj.Q(25), 1, obj.Q(29)};
                         {obj.Q(26), 0, obj.Q(22)};
                         {obj.Q(26), 1, obj.Q(5)};
                         {obj.Q(27), 0, obj.Q(32)};
                         {obj.Q(27), 1, obj.Q(26)};
                         {obj.Q(28), 0, obj.Q(39)};
                         {obj.Q(28), 1, obj.Q(33)};
                         {obj.Q(29), 0, obj.Q(17)};
                         {obj.Q(29), 1, obj.Q(8)};
                         {obj.Q(30), 0, obj.Q(37)};
                         {obj.Q(30), 1, obj.Q(15)};
                         {obj.Q(31), 0, obj.Q(30)};
                         {obj.Q(31), 1, obj.Q(4)};
                         {obj.Q(32), 0, obj.Q(36)};
                         {obj.Q(32), 1, obj.Q(31)};
                         {obj.Q(33), 0, obj.Q(17)};
                         {obj.Q(33), 1, obj.Q(9)};
                         {obj.Q(34), 0, obj.Q(3)};
                         {obj.Q(34), 1, obj.Q(15)};
                         {obj.Q(35), 0, obj.Q(34)};
                         {obj.Q(35), 1, obj.Q(26)};
                         {obj.Q(36), 0, obj.Q(38)};
                         {obj.Q(36), 1, obj.Q(35)};
                         {obj.Q(37), 0, obj.Q(3)};
                         {obj.Q(37), 1, obj.Q(4)};
                         {obj.Q(38), 0, obj.Q(37)};
                         {obj.Q(38), 1, obj.Q(19)};
                         {obj.Q(39), 0, obj.Q(18)};
                         {obj.Q(39), 1, obj.Q(20)};}; % State-transition rel.
            obj.H = {obj.Gamma{3}, obj.Gamma{3}, obj.Gamma{3}, obj.Gamma{3}, obj.Gamma{2}, ...
                obj.Gamma{2}, obj.Gamma{3}, obj.Gamma{2}, obj.Gamma{2}, obj.Gamma{1}, ...
                obj.Gamma{1}, obj.Gamma{1}, obj.Gamma{1}, obj.Gamma{3}, obj.Gamma{2},...
                obj.Gamma{2}, obj.Gamma{2}, obj.Gamma{2}, obj.Gamma{2}, obj.Gamma{2},...
                obj.Gamma{2}, obj.Gamma{2}, obj.Gamma{2}, obj.Gamma{2}, obj.Gamma{2}, ...
                obj.Gamma{2}, obj.Gamma{2}, obj.Gamma{1}, obj.Gamma{1}, obj.Gamma{2},...
                obj.Gamma{1}, obj.Gamma{2}, obj.Gamma{1}, obj.Gamma{2}, obj.Gamma{2},...
                obj.Gamma{2}, obj.Gamma{2}, obj.Gamma{2}, obj.Gamma{1}}; % Output map Q -> Gamma
            obj.h = obj.Gamma{3};
            obj.freq = 100;
        end
        
        function sensorSignal = filterSensedOmega(obj, omega)
            %FILTERSENSEDOMEGA Filters sensor with Moore machine
            %  Filters the signal from the sensor and producing a symbol T
            %  when it is certain that the sensor signalled an alarm, D
            %  when in doubt, and O when velocities are certainly within
            %  safe bounds.
            sensorOutput = senseOmega(obj, omega);
            for d = 1:size(obj.delta,1)
                if strcmp(obj.delta{d}{1}{1}, obj.q) && (obj.delta{d}{2} == sensorOutput)
                    obj.q = obj.delta{d}{3}{1};
                    sensorSignal = obj.H{str2num(obj.q)};
                    obj.h = sensorSignal;
                    return
                end
            end
        end 
        
%         function checkAlarm(obj, x, t) --unused, since system_freq = freq
%             %CHECKALARM Checks current alarm signal output
%             %  Since the sensor can only produce an output at 100 bits per
%             %  second, a check function is used to allow system to simly
%             %  check the current output while the SensorAlarm class updates
%             %  the output at the correct frequency.
%             
%         end

        function alarmSignal = checkAlarm(obj)
            %CHECKALARM Checks current alarm signal output
            %  Outputs a boolean: if output signal is == 'T'
            alarmSignal = strcmp(obj.h, 'T');
            if(alarmSignal)
                disp('Alarm Triggered!')
            end
        end
        
        function reset(obj)
            %RESET Resets the sensor
            %  Resets the sensor to initial consitions
            obj.flip_count = randi(9)-1; % Random number between 0 and 8
            obj.q = obj.Q_0; % Current state is the initial state
            obj.h = obj.Gamma{3};
        end
    end
    
    methods % (access = private)
        function sensorOutput = senseOmega(obj, omega)
            %SENSEOMEGA Senses if omega is within a safe bound
            %   Sensed is the angular velocity is within a safe bound, with
            %   low quality sensing added. Since the average error rate is
            %   less than or equel to 8 bits, but otherwise unspecified,
            %   the error is modeled by giving a constant chance
            %   (flip_prob) of bit flipping 8 bits after the previous flip.
            if abs(omega) < obj.W
                sensorOutput = 0;
            else
                sensorOutput = 1;
            end
            if obj.flip_count >= 8
                x = rand;
                if x < obj.flip_prob
                    sensorOutput = ~sensorOutput;
                    obj.flip_count = 0;
                    return;
                end
            end
            obj.flip_count = obj.flip_count + 1;
        end
    end
end

